# AT90S4433_evaluation_board

Simple board allowing to test codes for AT90S4433 with breadboard circuits.

![PCB front](photos/board_front.jpg) ![PCB back](photos/board_back.jpg)


## Assembly tips and instructions

* 22 pf capacitors for C1 and C2 (xtal1 xtal2)


## Example programs - building and flashing

First, install necessary tools:
```bash
sudo apt install avra avrdude gcc-avr avr-libc make
```

For program upload I used [USBasp programmer](https://www.fischl.de/usbasp/) with [avrdude CLI tool](https://github.com/avrdudes/avrdude).

:warning: **Remember to swith JP9 to 'ext' position before flashing**

### Assembly examples:

```bash
cd example_asm_software

# Build
avra <sketch_name>.asm

# Check USBasp connection - you can ignore invalid signature warning
avrdude -c usbasp -p 4433

# Flash
avrdude -c usbasp -p 4434 -F -U flash:w:<sketch_name>.hex:i
```

### C examples:

```bash
cd example_c_software

# Build by hand
avr-gcc -mmcu=at90s4433 <sketch_name>.c -o <sketch_name>.out
avr-objcopy -O ihex <sketch_name>.out <sketch_name>.hex

# And flash by hand
avrdude -c usbasp -p 4434 -F -U flash:w:<sketch_name>.hex:i

# Or use automated building and flashing
make TARGET=<sketch_name> flash
```

## Some clues and related projects:

* [midi-exiter-at90s4433](https://github.com/MicrochipTech/avrfreaks-projects/tree/main/projects/midi-exiter-at90s4433) - project

* [akwkombajn](https://www.elektroda.pl/rtvforum/topic1013015.html) - project
