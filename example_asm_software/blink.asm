.INCLUDE "4433def.inc"

    LDI   r16 ,low(ramend); initailizing stack pointer
    OUT   sp, r16

    LDI   r17, 0b11111111; setting PORTC as output
    OUT   DDRC, r17

main:
    RCALL led_on
    RCALL Delay_1_8sec
    RCALL led_off
    RCALL Delay_1_8sec
    RJMP  main

led_off:
    LDI   r17, 0b00000000
    OUT   PORTC, r17
    NOP
    RET

led_on:
    LDI   r17, 0b00010000
    OUT   PORTC, r17
    NOP
    RET

Delay_1_8sec:                 ; For CLK(CPU) = 1 MHz
    LDI     r16,   8       ; One clock cycle;
Delay1:
    LDI     r17,   125     ; One clock cycle
Delay2:
    LDI     r18,   250     ; One clock cycle
Delay3:
    DEC     r18            ; One clock cycle
    NOP                     ; One clock cycle
    BRNE    Delay3          ; Two clock cycles when jumping to Delay3, 1 clock when continuing to DEC

    DEC     r17            ; One clock cycle
    BRNE    Delay2          ; Two clock cycles when jumping to Delay2, 1 clock when continuing to DEC

    DEC     r16            ; One clock Cycle
    BRNE    Delay1          ; Two clock cycles when jumping to Delay1, 1 clock when continuing to RET
RET