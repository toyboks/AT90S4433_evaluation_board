/*
    Example of activating LED pin with button
    Based on polling approach
*/

.INCLUDE "4433def.inc"

    LDI   r16, low(ramend); initailizing stack pointer
    OUT   sp, r16

    LDI   r17, 0b11011111; setting PORTC as output except for button pin
    OUT   DDRC, r17

main:
    IN    r16, PINC
    ANDI  r16, 0b00100000
    BRNE  button_on
    RCALL led_off
    RJMP  main

button_on:
    RCALL led_on
    RJMP  main


led_off:
    LDI   r17, 0b00000000
    OUT   PORTC, r17
    RET

led_on:
    LDI   r17, 0b00010000
    OUT   PORTC, r17
    RET