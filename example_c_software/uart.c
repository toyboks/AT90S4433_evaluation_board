/*
  Sends something but not requested chars - probably because the oscillator has strange value.
*/

#define F_CPU 7372800UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

void USART_Init(void)
{
  UBRR = BAUD_PRESCALE;
  UBRRH = (BAUD_PRESCALE >> 8);

  UCSRB = ((1 << TXEN) | (0 << RXEN) | (1 << RXCIE));
}

void USART_SendByte(uint8_t u8Data)
{
  while ((UCSRA & (1 << UDRE)) == 0)
    ;
  UDR = u8Data;
}

int main()
{
  USART_Init();
  DDRC = 0xFF;
  PORTC = 0x00;
  sei();

  while (1)
  {
    PORTC = ~PORTC;
    _delay_ms(500);
    USART_SendByte((uint8_t)'a');
    PORTC = ~PORTC;
    _delay_ms(500);
    USART_SendByte((uint8_t)'b');
  }
  return 0;
}
