#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

ISR(TIMER0_OVF_vect)
{
    static int counter = 30;
    TCNT0 = 0;
    if(counter--) {
        return;
    }
    PORTC = ~PORTC;
    counter = 30;
}

int main(void)
{
    /* Set C pins (LED) as output */
    DDRC = 0xFF;
    PORTC = 0x00;

    TCNT0 = 0;
    TCCR0 = 0b00000101;
    TIMSK = 0b00000010;

    _delay_ms(500);

    sei(); /* Enable Global Interrupt */

    while (1);
}