#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

int main()
{
  DDRC = 0b11111111;

  while (1) {
    PORTC = 0b00010000;
    //PORTC |= (1 << PB0);
    _delay_ms(1000);
    PORTC = 0b00000000;
    //PORTC &= ~(1 << PB0);
    _delay_ms(1000);
  }
  return 0;
}