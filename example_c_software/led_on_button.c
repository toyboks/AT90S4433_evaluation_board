#define F_CPU 8000000UL

#include <avr/io.h>

int main() {
    DDRC = 0b11011111;

    while (1) {
        if (PINC & 0b00100000)
            PORTC = 0b00010000;
        else
            PORTC = 0b00000000;
    }
    
}