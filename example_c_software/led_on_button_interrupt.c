#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

ISR(INT0_vect)
{
	PORTC = ~PORTC;
	_delay_ms(500);
}

int main(void)
{
	/* Set all D pins (interrupt) as input and set pullups */
	DDRD = 0x00;
	PORTD = 0x00;

	/* Set C pins (LED) as output */
	DDRC = 0xFF;
	PORTC = 0x00;

	GIMSK = 0 << INT0;				 /* Set interrupt mask - enable interrupt on PD2 */
	MCUCR = 1 << ISC01 | 0 << ISC00; /* Trigger INT0 on rising edge */
	GIMSK = 1 << INT0;				 /* Set interrupt mask - enable interrupt on PD2 */

	sei(); /* Enable Global Interrupt */

	while (1);
}